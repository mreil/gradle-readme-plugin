package com.mreil.gradleplugins.readme

import com.mreil.gradleplugins.readme.template.ReadmeTemplateRenderer
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction

import javax.inject.Inject

class ReadmeGenerateTask extends DefaultTask {
    static final String TASK_NAME = "generateReadme"
    private final Project project

    @Inject
    ReadmeGenerateTask(Project project) {
        this.project = project
        group = "Readme"
        description = "Generate README"
    }

    @TaskAction
    void doTask() {
        def readme = project.file("README.md")
        if (readme.exists()) {
            getLogger().info("README exists. Skipping generation...")
        } else {
            readme.write(
                    new ReadmeTemplateRenderer(project.getName()).renderTemplate())
        }
    }
}
