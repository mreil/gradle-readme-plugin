package com.mreil.gradleplugins.readme

import org.gradle.api.Plugin
import org.gradle.api.Project

class ReadmePlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.tasks.create(ReadmeGenerateTask.TASK_NAME, ReadmeGenerateTask, project)
    }
}
