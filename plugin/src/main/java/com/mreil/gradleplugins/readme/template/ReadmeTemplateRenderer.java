package com.mreil.gradleplugins.readme.template;

import com.vladsch.flexmark.ast.Heading;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.collection.iteration.ReversiblePeekingIterator;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class ReadmeTemplateRenderer {
    private final String title;
    private final Configuration cfg;

    public ReadmeTemplateRenderer(String title) {
        this.title = title;

        cfg = new Configuration(Configuration.VERSION_2_3_28);
        cfg.setDefaultEncoding(StandardCharsets.UTF_8.toString());
        cfg.setTemplateLoader(new ClassTemplateLoader(this.getClass().getClassLoader(), ""));
    }

    public String renderTemplate() {

        HashMap<String, Object> model = new HashMap<>();
        model.put("title", title);

        String md;

        md = processFreemarker(model);

        validateMarkdown(md);

        return md;
    }

    private String processFreemarker(Map<String, Object> model) {
        try {
            Template template = cfg.getTemplate("README.template.md");
            StringWriter sw = new StringWriter();
            template.process(model, sw);
            return sw.toString();
        } catch (IOException | TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    private void validateMarkdown(String md) {
        Parser parser = Parser.builder().build();
        Document document = parser.parse(md);

        ReversiblePeekingIterator<Node> children = document.getChildren().iterator();
        matchHeading(children, "Install");
        matchHeading(children, "Usage");
        matchHeading(children, "Contributing");
        matchHeading(children, "License");
    }

    private void matchHeading(ReversiblePeekingIterator<Node> children, String name) {
        while (children.hasNext()) {
            Node n = children.next();
            if (n instanceof Heading) {
                Heading h = (Heading) n;
                if (h.getLevel() == 2 && h.getText().toString().equals(name)) {
                    return;
                }
            }
        }
        throw new IllegalStateException("Missing heading '" + name + "'");
    }

}
