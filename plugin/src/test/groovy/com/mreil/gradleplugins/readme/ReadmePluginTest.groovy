package com.mreil.gradleplugins.readme

import org.gradle.api.Project
import org.gradle.internal.impldep.org.apache.commons.lang.RandomStringUtils
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class ReadmePluginTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    Project project

    def setup() {
        project = ProjectBuilder.builder()
                .withProjectDir(testProjectDir.root)
                .withName(RandomStringUtils.random(20))
                .build()
    }

    def "Plugin is applied"() {
        given: "plugin is applied"
            project.plugins.apply(ReadmePlugin)

        expect: "plugin is in plugins"
            project.plugins.findPlugin(ReadmePlugin)
    }

    def "Task generates README"() {
        given: "plugin is applied"
            project.plugins.apply(ReadmePlugin)

        when: "task is run"
            def task = project.tasks.getByName(ReadmeGenerateTask.TASK_NAME)
            task.actions.each { a ->
                a.execute(task)
            }

        then: "README is written"
            def readme = project.file("README.md")
            readme.exists()
            readme.readLines().contains("# ${project.name}".toString())
    }
}
