package com.mreil.gradleplugins.readme.template

import spock.lang.Specification

class ReadmeTemplateRendererTest extends Specification {

    def "test render template"() {
        def title = "the-title"
        when:
            ReadmeTemplateRenderer renderer = new ReadmeTemplateRenderer(title)
            def rendered = renderer.renderTemplate()

        then:
            rendered != null
            rendered.contains(title)
    }
}
