# gradle-readme-plugin

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> [Gradle][gradle] plugin to add a readme.


# Install

    plugins {
        id "com.mreil.readme"
    }

 
# Usage

    ./gradlew generateReadme


# Changelog

See the Changelog [here][changelog].


# Maintainers

[Markus Reil][bb-mreil-com]


# Contributing

Feel free to dive in! [Open an issue][issues] or raise a [Pull Request][pr].


# License

[MIT][license]


[gradle]: https://gradle.org/
[bb-mreil-com]: https://bitbucket.org/mreil-com/
[pr]: ../../pull-requests/
[issues]: ../../issues/
[license]: LICENSE.txt
[changelog]: CHANGELOG.md
